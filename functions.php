<?php
/**
 * Utility Pro.
 *
 * @package      Utility_Pro
 * @link         http://www.carriedils.com/utility-pro
 * @author       Carrie Dils
 * @copyright    Copyright (c) 2015, Carrie Dils
 * @license      GPL-2.0+
 */

include get_stylesheet_directory() . '/includes/enqueue-assets.php';
require get_stylesheet_directory() . '/includes/google-fonts.php';
include get_stylesheet_directory() . '/includes/theme-config.php';
require get_stylesheet_directory() . '/includes/theme-defaults.php';
require get_stylesheet_directory() . '/includes/class-search-form.php';
include get_stylesheet_directory() . '/includes/widget-areas.php';
include get_stylesheet_directory() . '/includes/footer-nav.php';

add_filter( 'genesis_footer_creds_text', 'utility_pro_footer_creds' );
function utility_pro_footer_creds( $creds ) {

	return '[footer_copyright first="2015"] &middot; Designed With <i class="fa fa-heart"></i> By <a href="http://sbydigital.com">SBY Digital</a>';
}

