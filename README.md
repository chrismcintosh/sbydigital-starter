# UTILITY PRO MOD by SBY Digital CHILD THEME
http://sbydigital.com

## REQUIREMENTS
* This theme requires the Genesis Framework 2.0 or greater. The Genesis parent theme needs to be in the `wp-content/themes/` directory, but does not need to be activated.
* This theme requires WordPress 4.1 or greater.

## WIDGET AREAS
### Primary Sidebar
This is the primary sidebar if you are using the Content/Sidebar or Sidebar/Content Site Layout option.
### Utility Bar
This widgetized area appears above the header. You can use this area to communicate important information to your site visitors.
### Home Welcome
This is the welcome area at the top of the home page.
### Call to Action
This is the call to action area at the bottom of the home page.
### Footer #1
This is the first column of the footer section.
### Footer #2
This is the second column of the footer section.
### Footer #3
This is the third column of the footer section.

## Suggested Plugins

* [Better Font Awesome](https://wordpress.org/plugins/better-font-awesome/)
* [Genesis eNews Extended by Brandon Kraft](https://wordpress.org/plugins/genesis-enews-extended/)
* []()

To address accessibility issues in WordPress and the Genesis Framework, the following plugins are recommended:

* [Genesis Accessible](https://wordpress.org/plugins/genesis-accessible)
* [WP Accessibility](https://wordpress.org/plugins/wp-accessibility/)

## DOCUMENTATION & SUPPORT

You can find full documentation for the original version of Utility Pro at:

https://store.carriedils.com/support/documentation/

If you have questions or issues, please get in touch with our support team here:

https://store.carriedils.com/support/


A huge thank you to the following folks for providing translation files for this theme:

* Santiago Barrionuevo (es_ES, es_AR)
* Beth van Koetsveld (es_VE)
* Rian Reitveld (nl_NL)
* Vajrasar Goswami (hi_IN)
* Rosie Rodrigues (pt_BR)
* Luis Martins (pt_PT)
* Nir Rosenbaum (he_IL)
* Morten Rand-Hendriksen (nb_NO)
* Grégoire Noyelle (fr_FR)
* Gary Jones (en_GB)
* David Decker (de_DE)
* Fani Vasileva (bg_BG)

For more information on translating this theme, please check https://store.carriedils.com/translate/

## CHANGELOG

See [CHANGELOG.md](CHANGELOG.md).
